%{
#include<math.h>
%}
	int sum = 0,count=0;
%%
[0-9]+ {count++; sum += atoi(yytext) ; } 
%%
int yywrap()
{
	return 1;
}
main()
{
	yylex();
	printf("The count of all numbers = %d\n",count);
	printf("The sum of all numbers  = %d\n",sum);
}
/*
OBSERVATION
input :What are you 2 doing up there with 3 cats and 4 dogs?
ouput:
What are you  doing up there with  cats and  dogs?
The count of all numbers = 3
The sum of all numbers  = 9
*/