#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX_BUF 4

struct edge 
{
    int node;
    char inp;
    struct edge* next ;
};

struct graph
{
    int node ;
    struct edge* edges ;
}*nfa ;

int eclosure(int,struct graph*,int**) ;

int main(int argc, char** argv)
{
    int nQ,i,j = 0;
    int **epsc ;
    char buf[MAX_BUF] ;
    printf("Enter number of states : ") ;
    scanf("%d",&nQ) ;

    nfa = (struct graph*) malloc(sizeof(struct graph)*nQ) ; // Initialize nfa
    epsc = (int **) malloc(sizeof(int)* nQ) ; // Initialize a table to store epsilon closures, a row for a state
    struct edge *curr =  (struct edge*) malloc(sizeof(struct edge)) ; // Initialize a temporary edge
    for(i=0 ; i<nQ; i++)
    {
        printf("Enter edges for state %d in the following format \nNOTE: (q to skip)(e for epsilon):",i) ;
        printf("\nFormat : edge character,end state number\n") ;
        nfa[i].edges = (struct edge*) malloc(sizeof(struct edge)) ;
        curr = NULL ;
        strcpy(buf,"\0");
        while(scanf("%s",&buf) && strcmp(buf,"q"))
        {
            struct edge *new = (struct edge*) malloc(sizeof(struct edge)) ;
            sscanf(buf,"%c %*c %d",&new->inp,&new->node) ;
            new->next = NULL;
            if(curr==NULL) {nfa[i].edges = new ; curr = new ;} // for the first edge
            else{ curr->next = new ; curr = new ; }
            strcpy(buf,"\0") ;
        }
    }
    for (i=0; i<nQ ; i++)
    {
        epsc[i] = (int*)malloc(sizeof(int)*nQ) ;
        epsc[i][0] = -1 ; // -1 marks the end 
    }
   for (i=0 ; i<nQ ; i++)
    {
        if(epsc[i][0]==-1) eclosure(i,nfa,epsc);
        printf("\nE(%d) -> ",i);
        while(epsc[i][j] != -1)
            printf("%d ",epsc[i][j++]) ;
        j = 0;
    }
    return 0;
}
int eclosure(int node, struct graph* nfa, int** epsc)
{
    if(epsc[node][0] != -1) return 0; // already eclosure saved in epsc
    int cur = 0,*i;
    struct edge* cur_edg = (struct edge*) malloc(sizeof(struct edge)) ;
    epsc[node][cur++] = node ;
    epsc[node][cur] = -1 ;
    cur_edg = nfa[node].edges ;
    while(cur_edg!=NULL){ // if there is an edge
        if(cur_edg->inp=='e'){ // if the edge is an epsilon transition
            eclosure(cur_edg->node, nfa, epsc) ; // do its eclosure
            i = epsc[cur_edg->node]; // and add its eclosure to this eclosure
            while(*i!=-1){
                epsc[node][cur++] = *i ;
                epsc[node][cur] = -1 ;
                i = ++i ;
            }
        }
        cur_edg = cur_edg->next ; // move to next edge if any
    }
    return 0;
}