#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>

#define INP_SIZE 2000 // in characters

int is_valid_idn(char*, int) ; 
/* helper functions*/
int is_a(char*, char**, int) ; // "arg1" is in arg2[]
void tokenify( char*, int, char** , char**, char**, int*) ;

int main()
{
     int i, tkn_cnt = 0 ;
     char inp_str[INP_SIZE] = {"void main(int argc, char** argv){\nint a = 0, b=0,c;\nif(a==b) c = 1 ;\nprintf(\"\%d\",c) ;\nreturn 0;\n}"} ;
     char** tkn_lst = (char**) malloc(100 * sizeof(char*)) ;
     char *keywords[] = { "printf", "scanf", "if"  , "else", "while", "int"   , "float", "char", "void" };
     char *delimiters[] = { ";", ",", "(", ")", "[", "]", "{", "}", " ","\n" } ;
     char *ops[] = { "/", "*", "%", "+", "-", "!", "<", ">", "==", "!=" } ;
     printf("%s\n",inp_str) ;
     /* first obtain the tokens */
     tokenify(inp_str, strlen(inp_str),delimiters, ops, tkn_lst,&tkn_cnt);
     /* identifier is valid if is_valid_idn is true and all is_a(str,keywords,len(keywords)) is false */
     for( i = 0 ; i < tkn_cnt; i++)
          printf("\n%s",tkn_lst[i]);
     return 0 ;

     
}

/* tokenize str of length based on delimiters delm and operators ops into a list whose length returned to tkn_cnt*/
void tokenify(char* str, int length, char** delm, char** ops, char** list, int *tkn_cnt)
{
    /* put every letter to buffer until delim */
    char buf[20],ch[2] ;
    int i, j = 0 , k = 0 , delm_sz ;
    //for (delm_sz = 0 ; delm[delm_sz] != NULL; delm_sz++) ; // find no. of delimiters
    for( i = 0 ; i < length ; i++ ) {
          ch[0] = str[i] ; ch[1] = '\0'; // make a string out of a char
	  //if( is_a( ch, ops, 10)) continue;
	  if( is_a( ch, delm, 10 ) )
	  { 
		  if(!strlen(buf)) continue;
		  list[j] = (char*) malloc(10);
		  strcpy(list[j++],buf); 
		  k = 0 ; 
		  buf[0] = '\0' ; 
		  (*tkn_cnt)++ ;
	       } 
          else
	  	{  buf[k++] = str[i] ; buf[k]='\0';}
    }
}

int is_valid_idn(char* str, int length)
{
     int i;
     if( !(isalpha(str[0]) || str[0] == '_') )
          return 0 ;
     for( i = 1 ; i < length ; i++)
          if( !(isalnum(str[i]) || str[i] == '_' || str[i] == '-' ))
               return 0 ;
     return 1 ;
}

int is_a(char* str, char** list, int length)
{
     int i ; 
     for( i = 0 ; i < length ; i++ )
          if( !strcmp(str, list[i]) )
               return 1;
     return 0;
}
